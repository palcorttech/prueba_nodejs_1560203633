/**
 * @author 			MANUEL ALEJANDRO PINZON SALGADO
 * @description 	Inicializacion del servidor y configuraciones
 */

const adapter = require('./server/adapter');
const config = require('./config');
const include = require('include-all')
const path = require('path');
const server = require('./server/server');

const dir = `${process.env.PWD}/back`;

global.models = {}

models = include({
	dirname: path.join(dir, 'models'),
	filter: /(.+)\.js$/,
	excludeDirs: /^\.(git|svn)$/
});

global.controllers = include({
	dirname: path.join(dir, 'controllers'),
	filter: /(.+)\.js$/,
	excludeDirs: /^\.(git|svn)$/
});

adapter( models, () => {
	// Inicializar el servidor
	const app = require('./server/app')(config);
	server(app, config);
});
