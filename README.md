## Prueba - Desarrollo Back de tienda de videos

Elaborada por: Manuel Alejandro Pinzon Salgado

- Restauracion base de datos:
pg_restore -d postgres dvdrental.tar -c -U postgres

- Ejecutar
node index.js
npm start


# Estructura de vistas

Buscar pelicula.
  /film           query: id  title        

En caso de tener existencias ir a vista o vista dinamica con React, Angular, etc. Buscar Usuario.
  /customer       query: id  first_name  last_name  email

En caso de no existir el usuario. Ir a vista nuevo usuario.
  /country        Lista todos los paises
  /city           query: country              Lista todas las ciudades de determinado pais


# TODO

Falta:
- Crear customer
- Rentar pelicula
