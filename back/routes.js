module.exports.routes = {

	'GET /film': {
		controller: 'Film',
		action: 'read'
	},

  'GET /customer': {
		controller: 'Customer',
		action: 'read'
	},

  'GET /city': {
		controller: 'City',
		action: 'read'
	},

  'GET /country': {
		controller: 'Country',
		action: 'read'
	}

};
