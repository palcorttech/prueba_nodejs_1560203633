const Sequelize = require('sequelize')

module.exports = {
  country_id: { type: Sequelize.INTEGER, primaryKey: true },
  country: Sequelize.STRING,
  last_update: Sequelize.TIME,
}
