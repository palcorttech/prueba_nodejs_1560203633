const Sequelize = require('sequelize')

module.exports = {
  city_id: { type: Sequelize.INTEGER, primaryKey: true },
  city: Sequelize.STRING,
  country_id: Sequelize.SMALLINT,
  last_update: Sequelize.TIME,
}
