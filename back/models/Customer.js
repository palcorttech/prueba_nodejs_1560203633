const Sequelize = require('sequelize')

module.exports = {
  customer_id: { type: Sequelize.INTEGER, primaryKey: true },
  store_id: Sequelize.SMALLINT,
  first_name: Sequelize.STRING,
  last_name: Sequelize.STRING,
  email: Sequelize.STRING,
  address_id: Sequelize.SMALLINT,
  activebool: Sequelize.BOOLEAN,
  create_date: Sequelize.DATE,
  last_update: Sequelize.TIME,
  active: Sequelize.INTEGER,
}
