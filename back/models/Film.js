const Sequelize = require('sequelize')

module.exports = {
  film_id: { type: Sequelize.INTEGER, primaryKey: true },
  title: Sequelize.STRING,
  description: Sequelize.STRING,
  release_year: Sequelize.SMALLINT,
  language_id: Sequelize.SMALLINT,
  rental_duration: Sequelize.SMALLINT,
  rental_rate: Sequelize.INTEGER,
  length: Sequelize.SMALLINT,
  replacement_cost: Sequelize.INTEGER,
  //rating: Sequelize.,
  last_update: Sequelize.TIME,
  special_features: Sequelize.STRING,
  fulltext: Sequelize.STRING,
}
