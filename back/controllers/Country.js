/**
 * @author MANUEL ALEJANDRO PINZON SALGADO
 * @description Controlador Country
*/

const Sequelize = require('sequelize')
const Op = Sequelize.Op

module.exports = {

	read: async (req, res, next) => {
    try {
      let data = await Country.findAll()

      return res.json(data)
    } catch(err) {
      console.log(err)
      return res.json([])
    }
	}

};
