/**
 * @author MANUEL ALEJANDRO PINZON SALGADO
 * @description Controlador Film
*/

const Sequelize = require('sequelize')
const Op = Sequelize.Op

module.exports = {

	read: async (req, res, next) => {
    try {
      const { id, title } = req.query
      let data = [];

      if(id) {
        data = await Film.findByPk(id)
      } else if(title) {
        data = await Film.findAll({ where: { title: { [Op.like]: `%${title}%` } } })
      } else {
        data = await Film.findAll()
      }

      return res.json(data)
    } catch(err) {
      console.log(err)
      return res.json([])
    }
	}

};
