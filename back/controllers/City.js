/**
 * @author MANUEL ALEJANDRO PINZON SALGADO
 * @description Controlador City
*/

const Sequelize = require('sequelize')
const Op = Sequelize.Op

module.exports = {

	read: async (req, res, next) => {
    try {
      const { country } = req.query

      if(!country) {
        return res.json([])
      }

      let data = await City.findAll({ where: { country_id: country } })
      return res.json(data)
    } catch(err) {
      console.log(err)
      return res.json([])
    }
	}

};
