/**
 * @author MANUEL ALEJANDRO PINZON SALGADO
 * @description Controlador Customer
*/

const Sequelize = require('sequelize')
const Op = Sequelize.Op

module.exports = {

	read: async (req, res, next) => {
    try {
      const { id, email, first_name = '', last_name = '' } = req.query
      let data = [];

      //Masked Bubble
      //Others Soup
      if(id) {
        data = await Customer.findByPk(id)
      } else if(email) {
        data = ( await Customer.findAll({ where: { email } }) )[0]
      } else if(first_name || last_name) {
        data = await Customer.findAll({ where: { first_name: { [Op.like]: `%${first_name}%` }, last_name: { [Op.like]: `%${last_name}%` } } })
      } else {
        data = await Customer.findAll()
      }

      return res.json(data)
    } catch(err) {
      console.log(err)
      return res.json([])
    }
	}

};
