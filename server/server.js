/**
 * @author 			MANUEL ALEJANDRO PINZON
 * @description 	Configuracion de servidor
 */

module.exports = function (app, config) {
	const port = config.port;
	app.set('port', port);

	let server = null;
  let http = require('http');
  server = http.createServer(app);

	server.listen(port, () => {
		console.log('Servidor corriendo en el puerto: ' + server.address().port);
	});
}
