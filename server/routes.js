/**
 * @author 			MANUEL ALEJANDRO PINZON
 * @description 	Configuracion del router para los servicios REST
 */

const router = require('express').Router();
const routes = require(`${process.env.PWD}/back/routes`).routes;

let method;

for (let key in routes) {
	let json = routes[key];

	method = key.split(' ').filter(Boolean);

	if(method.length === 2){
		let m = method[0].toLowerCase();

		if (m === 'get' || m === 'post' || m === 'put' || m === 'delete') {
			let controlador = global.controllers[json.controller];

			if(!controlador[json.action]) {
				throw new Error('Rutas mal elaboradas');
			}

			router[m](method[1], controlador[json.action]);
		} else {
			throw new Error('Rutas mal elaboradas');
		}
	} else {
		throw new Error('Rutas mal elaboradas');
	}
}

module.exports = router;
