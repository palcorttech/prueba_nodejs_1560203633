/**
 * @author 			MANUEL ALEJANDRO PINZON
 * @description 	Configuracion del servidor express
 */

const bodyParser = require('body-parser');
const express = require('express');

const routes = require('./routes');

let app = express();

module.exports = function(config) {
	app.set('trust proxy', 1);

	app.use(bodyParser.json({ limit: '1mb'}));
	app.use(bodyParser.urlencoded({ limit: '1mb', extended: false }));
	app.use('/', routes);

	app.use(function(req, res, next) {
		let err = new Error('Not Found');

		err.status = 404;
		res.status(404);

		return res.send('404');
	});

	app.use(function(err, req, res) {
		let num = err.status || 500;

		err.status = num;
		res.status(num);

    return res.send(num);
	});

	return app;
};
