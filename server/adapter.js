/**
 * @author 			MANUEL ALEJANDRO PINZON SALGADO
 * @description 	Inicializacion del orm de base de datos
 */

const Sequelize = require('sequelize')

module.exports = function(models, callback) {
	const sequelize = new Sequelize('postgres', 'postgres', 'admin123', {
	  host: '172.50.50.1',
	  dialect: 'postgres'
	})

	sequelize.authenticate()
  .then(() => {
    for( let key in models) {
			let model = sequelize.define(key.toLowerCase(), models[key], { freezeTableName: true, timestamps: false, })

			global[key] = model
			global.models[key] = model
		}

		callback()
  })
  .catch(err => {
    console.log(err)
  })
}
